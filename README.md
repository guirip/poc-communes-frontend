
# README #

## Start

You must define an env var to set the backend url:

`REACT_APP_BACKEND_URL=http://localhost:3000 npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

NB: The port might be different (e.g 3001) if the backend is already running on port 3000.