
import styled from 'styled-components'
import PropTypes from 'prop-types';
// import Box from '../common/Box';

/*
const StyledBoxx = styled(Box)`
  flex: 0 0 49%;
  display: flex;
`*/
const StyledBox = styled.div`
  flex: 1 0 42%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0.3em 0.5em;
  background-color: #d1daeb;
  border-radius: 5px;
`

const Title = styled.h2`

`
const ResultStatus = styled.div`
  margin: .5em;
  padding: .6em 1em;
  width: -webkit-fill-available;
  color: white;
  background-color: ${props => props.hasResults ? '#7ebd7e' : '#bf7575'}
`
const CitiesContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
  margin-top 1em;
  width: 100%;
  overflow: auto;
`
const City = styled.div`
  flex: 0 0 40%;
  display: flex;
  justify-content: space-between;
  margin: .5em;
  padding: 0.8em;
  background-color: #5b5b5b;
  color: white;
`
const PostalCode = styled.span`
  opacity: .5;
`

function LocatedSearchResults({ label, cities }) {
  const hasResults = Array.isArray(cities) && cities.length > 0;

  return (
    <StyledBox>
      <Title>{label}</Title>

      <ResultStatus hasResults={hasResults}>{
        hasResults
          ? `${cities.length} ville${cities.length > 1 ? 's' : ''} correspondant au text saisi`
          : `Aucun ville correspondant au texte saisi`
      }</ResultStatus>

      { hasResults &&
        <CitiesContainer>
          { cities.map(renderCity) }
        </CitiesContainer>
      }
    </StyledBox>
  );
}

const renderCity = (commune) => (
  <City key={commune._id}>
    <span>{commune.nomCommune}</span>
    <PostalCode>{commune.codePostal}</PostalCode>
  </City>
);

LocatedSearchResults.propTypes = {
  label: PropTypes.string.isRequired,
  cities: PropTypes.array.isRequired,
}

export default LocatedSearchResults;