
import { useContext, useState } from 'react';
import styled from 'styled-components';
import SearchContext from './SearchContext';
import Box from '../common/Box';

const Text = styled.span`
  margin-right: .7em;
  font-size: 1.8em;
`
const StyledInput = styled.input`
  flex-grow: 1;
  padding: .3em;
  font-size: 1.6em;
  border: 0;
  border-radius: 5px;
`

const BACKEND_CALL_DELAY = 300;

function SearchBar() {

  const { performSearch } = useContext(SearchContext);
  const [searched, setSearched] = useState('');
  const [timeoutId, setTimeoutId] = useState(null);

  function onUserInput({ target: {value} }) {
    if (value.length === 1) {
      value = value.toUpperCase();
    }
    setSearched(value);

    // Clear results when input is empty
    if (value === '') {
      performSearch(null);
    } else {
      // Debounce to avoid calling the backend for every typed character
      if (timeoutId) {
        window.clearTimeout(timeoutId);
        setTimeoutId(null);
      }
      setTimeoutId(window.setTimeout(_performSearch, BACKEND_CALL_DELAY, value));
    }
  }

  function _performSearch(value) {
    performSearch(value);
    setTimeoutId(null);
  }

  return (
    <Box>
      <Text>Je recherche...</Text>
      <StyledInput
        type="text"
        value={searched}
        placeholder="...une ville"
        onInput={onUserInput}
      />
    </Box>
  );
}

SearchBar.propTypes = {};

export default SearchBar;