
import { useContext } from 'react';
import styled from 'styled-components';
import SearchContext from './SearchContext';
import LocatedSearchResults from './LocatedSearchResults';

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: .8em;
  max-height: 88vh;

  & > div:first-child {
    margin-right: .8em;
  }
`

function SearchResults() {
  const {results} = useContext(SearchContext);
  return (
    !results
      ? null
      : (
        <Wrapper>
          <LocatedSearchResults
            label="Villes de métropole"
            cities={results.metropole}
          />
          <LocatedSearchResults
            label="Villes d'outre-mer"
            cities={results.domToms}
          />
        </Wrapper>
      )
  );
}

SearchResults.propTypes = {};

export default SearchResults;