
import styled from 'styled-components';

const StyledBox = styled.div`
  display: flex;
  align-items: center;
  padding: 0.3em 0.8em;
  background-color: #d1daeb;
  border-radius: 5px;
`;

function Box({ children }) {
  return (
    <StyledBox>{ children }</StyledBox>
  );
}

export default Box;
