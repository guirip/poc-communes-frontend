
import { useState } from 'react';
import styled from 'styled-components';
import SearchContext from './search/SearchContext';
import SearchBar from './search/SearchBar';
import SearchResults from './search/SearchResults';
import { getCommunesStartingWith } from './api/Communes';
import './App.css';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1em;
  background-color: #f4f6ff;
`

function App() {

  const [results, setResults] = useState(null);

  async function performSearch(value) {
    setResults(
      !value
        ? null
        : await getCommunesStartingWith(value)
    );
  }

  return (
    <SearchContext.Provider value={{ performSearch, results }}>
      <Container>
        <SearchBar />
        <SearchResults />
      </Container>
    </SearchContext.Provider>
  );
}

export default App;
