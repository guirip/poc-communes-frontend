
const { REACT_APP_BACKEND_URL:BACKEND_URL } = process.env;
const COMMUNES_CONTROLLER = '/communes';

/**
 * Find communes starting with given characters
 * @param  {string} value
 */
export async function getCommunesStartingWith(value) {
  const response = await fetch(`${BACKEND_URL}${COMMUNES_CONTROLLER}/${value}`, {
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
  });
  return await response.json();
}
